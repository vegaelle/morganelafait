from django.db import models
from django.utils.translation import gettext as _

from modelcluster.fields import ParentalKey
from wagtail.models import Page, Orderable
from wagtail.admin.panels import (
    FieldPanel, InlinePanel, MultiFieldPanel,
    FieldRowPanel
)
from wagtail.fields import RichTextField, StreamField
from wagtail.search import index
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField

from ..blocks import (
    HeroBlock, CtaBlock,
    ServicesBlock, PortfolioBlock, ClientsBlock, FeaturesBlock, TestimonialsBlock,
    AboutBlock, TeamBlock, PricingBlock, InlineBlock, ParagraphBlock, FAQBlock
)


class HomePage(Page):
    body = StreamField([
        ("hero", HeroBlock()),
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
    ], blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]    

    search_image = models.ForeignKey("wagtailimages.Image", on_delete=models.SET_NULL,
                                     null=True, blank=True)
    search_image_alt = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name=_("Image alternative text"))
    promote_panels = Page.promote_panels + [
        FieldPanel("search_image"),
        FieldPanel("search_image_alt")
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
        index.SearchField("search_image_alt"),
    ]

    class Meta:
        verbose_name = _("Home Page")


class SimplePage(Page, Orderable):
    body = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("features", FeaturesBlock()),
        ("about", AboutBlock()),
        ("team", TeamBlock()),
        ("pricing", PricingBlock()),
        ("faq", FAQBlock()),
        ("inline", InlineBlock()),
        ("paragraph", ParagraphBlock()),
        
    ], blank=True)
 
    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]    

    search_image = models.ForeignKey("wagtailimages.Image", on_delete=models.SET_NULL,
                                     null=True, blank=True)
    search_image_alt = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name=_("Image alternative text"))
    promote_panels = Page.promote_panels + [
        FieldPanel("search_image"),
        FieldPanel("search_image_alt")
    ]
    
    search_fields = Page.search_fields + [
        index.SearchField('body'),
        index.SearchField("search_image_alt"),
    ]

    class Meta:
        verbose_name = _("Simple Page")


class FormField(AbstractFormField, Orderable):
    page = ParentalKey("FormPage", on_delete=models.CASCADE, related_name="form_fields")


class FormPage(AbstractEmailForm, Orderable):
    intro = RichTextField()
    button_text = models.CharField(default=_("Send"), max_length=100)
    thank_you = RichTextField(verbose_name=_("Confirmation text"))

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel("intro"),
        InlinePanel("form_fields", label="Form Fields"),
        FieldPanel("button_text"),
        FieldPanel("thank_you"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    search_image = models.ForeignKey("wagtailimages.Image", on_delete=models.SET_NULL,
                                     null=True, blank=True)
    search_image_alt = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name=_("Image alternative text"))
    promote_panels = Page.promote_panels + [
        FieldPanel("search_image"),
        FieldPanel("search_image_alt")
    ]

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField("search_image_alt"),
    ]

    class Meta:
        verbose_name = _("Form Page")

