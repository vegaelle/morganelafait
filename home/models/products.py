from django.shortcuts import redirect
from django.db import models
from django.utils.translation import gettext as _
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib import messages

from modelcluster.fields import ParentalKey
from wagtail.models import Page, Orderable, RevisionMixin, ClusterableModel
from wagtail.admin.panels import FieldPanel, InlinePanel, PageChooserPanel
from wagtail.blocks import StructBlock, CharBlock, FloatBlock, ListBlock, ChoiceBlock
from wagtail.fields import RichTextField, StreamField
from wagtail.search import index
from wagtail.contrib.forms.models import AbstractFormField
from wagtail.snippets.models import register_snippet

from shop.models import Basket
from ..blocks import (
    AltImageBlock, CtaBlock,
    ServicesBlock, PortfolioBlock, ClientsBlock, FeaturesBlock, TestimonialsBlock,
    AboutBlock, TeamBlock, PricingBlock, InlineBlock, ParagraphBlock, FAQBlock
)

USER_VALUE_WIDGETS = (
    ('text', _("Text")),
    ('int', _("Integer")),
    ('float', _("Float")),
    ('date', _("Date")),
    ('color', _("Color")),
)


class FormSnippetField(AbstractFormField, Orderable):
    page = ParentalKey("FormSnippet", on_delete=models.CASCADE, related_name="form_fields")


@register_snippet
class FormSnippet(RevisionMixin, index.Indexed, ClusterableModel):
    name = models.CharField(verbose_name=_("Name"), unique=True, max_length=50)
    description = RichTextField(verbose_name=_("Description"))
    _revisions = GenericRelation("wagtailcore.Revision",
                                 related_query_name="form_snippet")

    panels =  [
        FieldPanel("name"),
        FieldPanel("description"),
        InlinePanel("form_fields", label="Form Fields"),
    ]

    search_fields = [
        index.SearchField("name"),
        index.SearchField("description"),
    ]

    class Meta:
        verbose_name = _("Form Snippet")

    @property
    def revisions(self):
        return self._revisions


class OptionValue(StructBlock):
    label = CharBlock(label=_("Option value"))
    code = CharBlock(label=_("Value code"))
    price_modifier = FloatBlock(label=_("Price modifier"), required=False)


class UserValueOption(StructBlock):
    label = CharBlock(label=_("Option name"))
    widget = ChoiceBlock(USER_VALUE_WIDGETS, default=USER_VALUE_WIDGETS[0][0]),
    code = CharBlock(label=_("Value code"))
    price_modifier = FloatBlock(label=_("Price modifier if set"), required=False)


class AbstractProductPage(Page, Orderable):
    short_description = RichTextField()
    body = RichTextField()
    code = models.CharField(max_length=50, unique=True,
                            verbose_name=_("Product unique code"))
    base_price = models.FloatField(verbose_name=_("Base price"), default=0)
    options = StreamField([
        ("single_choice", ListBlock(OptionValue, label=_("Single choice option"))),
        ("multi_choice", ListBlock(OptionValue, label=_("Multi choice option"))),
        ("user_value_choice", UserValueOption(label=_("User Value Choice"))),
    ], null=True, blank=True)
    allow_quantity = models.BooleanField(
        verbose_name=_("Allow order a specific quantity"), default=True)
    user_info = models.ForeignKey(FormSnippet, verbose_name=_("User info form"),
                                  null=True, blank=True, on_delete=models.SET_NULL)
    order_page = models.ForeignKey("wagtailcore.Page",
                                   verbose_name=_("Order page"),
                                   on_delete=models.SET_NULL, null=True,
                                   blank=True,
                                   related_name="used_as_order_page_by")

    images = StreamField([
        ("image", AltImageBlock()),
    ])
    attributes_title = models.CharField(max_length=50, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("code"),
        FieldPanel("base_price"),
        FieldPanel("short_description"),
        FieldPanel("body"),
        FieldPanel("images"),
        FieldPanel("options"),
        FieldPanel("user_info"),
        PageChooserPanel("order_page"),
        FieldPanel("attributes_title"),
        InlinePanel("attributes", label=_("Attributes")),
    ] 

    search_image = models.ForeignKey("wagtailimages.Image", on_delete=models.SET_NULL,
                                     null=True, blank=True)
    search_image_alt = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name=_("Image alternative text"))
    promote_panels = Page.promote_panels + [
        FieldPanel("search_image"),
        FieldPanel("search_image_alt")
    ]

    search_fields = Page.search_fields + [
        index.SearchField("short_description"),
        index.SearchField("body"),
        index.SearchField("search_image_alt"),
    ]

    parent_page_types = ["home.HomePage", "home.SimplePage",
                         "home.ProductCategoryPage"]
    subpage_types = ["home.FormPage"]

    @property
    def price(self):
        """
        Calculates the total price of the product, with the base price and options
        """
        return self.base_price

    def get_attributes(self):
        return self.attributes.all()

    def first_image(self):
        try:
            return self.images[0].value
        except IndexError:
            pass

    def build_add_to_cart_form(self):
        pass

    def serve(self, request, *args, **kwargs):
        if request.method == 'POST' and 'add_to_cart' in request.POST:
            # add this to basket
            basket, _created = Basket.objects.get_or_create_from_request(request)
            basket.add(self)
            messages.success(request, _("%s has been added to your cart") % self)
            return redirect(request.get_full_path())
        return super().serve(request, *args, **kwargs)

    class Meta(Page.Meta):
        abstract = True
        verbose_name = _("Product Page")


class ProductPage(AbstractProductPage):
    pass


class ProductAttribute(Orderable):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    product = ParentalKey(ProductPage, on_delete=models.CASCADE,
                          related_name="attributes")

    panels = [
        FieldPanel("name"),
        FieldPanel("value"),
    ]

    class Meta:
        verbose_name = _("Product Attributes")
        unique_together = (
            ("name", "product"),
        )


class ProductCategoryPage(Page, Orderable):
    body = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("features", FeaturesBlock()),
        ("about", AboutBlock()),
        ("team", TeamBlock()),
        ("pricing", PricingBlock()),
        ("faq", FAQBlock()),
        ("inline", InlineBlock()),
        ("paragraph", ParagraphBlock()),
        
    ], blank=True)
    attribute_name = models.CharField(
        max_length=200,
        help_text=_("Only display products that have this attribute"))
    attribute_value = models.CharField(
        max_length=200, blank=True,
        help_text=_("Only show product that have this value for the given attribute. "
                    "Set an empty value to show every product that have this attribute."))
    show_only_children = models.BooleanField(
        verbose_name=_("Show only children products"),
        default=False,
    )


    content_panels = Page.content_panels + [
        FieldPanel("body"),
        FieldPanel("attribute_name"),
        FieldPanel("attribute_value"),
        FieldPanel("show_only_children"),
    ]    

    search_image = models.ForeignKey("wagtailimages.Image", on_delete=models.SET_NULL,
                                     null=True, blank=True)
    search_image_alt = models.CharField(max_length=200, null=True, blank=True,
                                        verbose_name=_("Image alternative text"))
    promote_panels = Page.promote_panels + [
        FieldPanel("search_image"),
        FieldPanel("search_image_alt"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
        index.SearchField('attribute_name'),
        index.SearchField("search_image_alt"),
    ]

    parent_page_types = ["home.HomePage", "home.SimplePage", "home.ProductCategoryPage"]
    subpage_types = ["home.productCategoryPage", "home.ProductPage"]

    def get_context(self, request):
        ctx = super().get_context(request)
        qs = ProductPage.objects.all()
        if self.show_only_children:
            qs = qs.descendant_of(self)
        ctx["products"] = qs.public().live().filter(
            attributes__name__iexact=self.attribute_name
        )
        if self.attribute_value:
            ctx["products"] = ctx["products"].filter(
                attributes__value__icontains=self.attribute_value
            )
        products = []
        for prod in ctx["products"]:
            cur_attr = prod.attributes.get(name__iexact=self.attribute_name).value
            products.append((cur_attr, prod))
        ctx["products"] = products
        return ctx


    class Meta:
        verbose_name = _("Product Category Page")
