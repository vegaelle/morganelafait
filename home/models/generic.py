from django.db import models
from django.utils.translation import gettext as _

from wagtail.admin.panels import (
    FieldPanel, PageChooserPanel, TabbedInterface, ObjectList
)
from wagtail.fields import RichTextField, StreamField
from wagtail.blocks import PageChooserBlock
from wagtail.contrib.settings.models import  BaseSiteSetting, register_setting
from wagtail_color_panel.fields import ColorField
from wagtail_color_panel.edit_handlers import NativeColorPanel

from ..blocks import (
    SocialLinkBlock, ContactBlock
)


@register_setting
class CustomSiteSettings(BaseSiteSetting):
    logo = models.ForeignKey("wagtailimages.Image", verbose_name=_("Logo"),
                             on_delete=models.CASCADE, null=True,
                             blank=True, related_name="used_as_logo_by")
    favicon = models.ForeignKey("wagtailimages.Image", verbose_name=_("Favicon"), on_delete=models.CASCADE, null=True,
                                blank=True, related_name="used_as_favicon_by")
    contacts = StreamField([
        ("contact", ContactBlock()),
    ], blank=True)
    socials = StreamField([
        ("social_link", SocialLinkBlock()),
    ], verbose_name=_("Contacts"), blank=True)
    footer_links_header = models.CharField(_("Footer Links Header"), max_length=50, blank=True)
    footer_links = StreamField([
            ("footer_link", PageChooserBlock(icon="link")),
    ], blank=True, verbose_name=_("Footer links"))
    footer_text = RichTextField(blank=True)

    default_color = ColorField(_("Default color"))
    newsletter_header = models.CharField(verbose_name=_("Newsletter block header"),
                                        max_length=100, default=_("Newsletter"))
    newsletter_desc = RichTextField(verbose_name=_("Newsletter block description"),
                                       blank=True, default="")
    mailing_confirm_title = models.CharField(
        verbose_name=_("Subscription confirmation title"), max_length=100,
        default=_("Thanks for your subscription!")
    )
    mailing_confirm_text = RichTextField(verbose_name=_("Subscription confirmation text"),
                                         blank=True, default="")
    mailing_valid_title = models.CharField(
        verbose_name=_("Mail validation title"), max_length=100,
        default=_("Confirm your email")
    )
    mailing_valid_text = RichTextField(verbose_name=_("Mail validation text"),
                                         blank=True, default="")
    mailing_valid_confirm_title = models.CharField(
        verbose_name=_("Mail validation confirmation title"), max_length=100,
        default=_("Your email is now confirmed!")
    )
    mailing_valid_confirm_text = RichTextField(
        verbose_name=_("Mail validation confirmation text"),
        blank=True, default="")
    mailing_confirm_page = models.ForeignKey("wagtailcore.Page",
                                             verbose_name=_("Mail confirmation landing page"),
                                             on_delete=models.CASCADE, null=True,
                                             blank=True,
                                             related_name="used_as_landing_page_by")
    mailing_confirm_button_text = models.CharField(
        verbose_name=_("Mail confirmation landing button text"), max_length=100,
        default=_("Back to homepage")
    )
    mailing_unsubscribe_title = models.CharField(
        verbose_name=_("Mail unsubscription title"), max_length=100,
        default=_("Do you really want to unsubscribe?")
    )
    mailing_unsubscribe_text = RichTextField(verbose_name=_("Mail unsubscribe text"),
                                         blank=True, default="")
    mailing_unsubscribe_confirm_title = models.CharField(
        verbose_name=_("Mail unsubscription confirmation title"), max_length=100,
        default=_("Your email is now unsubscribed.")
    )
    mailing_unsubscribe_confirm_text = RichTextField(
        verbose_name=_("Mail unsubscription confirmation text"),
        blank=True, default="")
    mailing_confirm_page = models.ForeignKey("wagtailcore.Page",
                                             verbose_name=_("Mail confirmation landing page"),
                                             on_delete=models.CASCADE, null=True,
                                             blank=True,
                                             related_name="used_as_landing_page_by")
    mailing_confirm_button_text = models.CharField(
        verbose_name=_("Mail confirmation landing button text"), max_length=100,
        default=_("Back to homepage")
    )
    

    generic_panels = [
        FieldPanel("logo"),
        FieldPanel("favicon"),
        FieldPanel("contacts"),
        FieldPanel("socials"),
        FieldPanel("footer_text"),
        FieldPanel("footer_links_header"),
        FieldPanel("footer_links"),
    ]
    mailing_panels = [
        NativeColorPanel("default_color"),
        FieldPanel("newsletter_header"),
        FieldPanel("newsletter_desc"),
        FieldPanel("mailing_confirm_title"),
        FieldPanel("mailing_confirm_text"),
        FieldPanel("mailing_valid_title"),
        FieldPanel("mailing_valid_text"),
        FieldPanel("mailing_valid_confirm_title"),
        FieldPanel("mailing_valid_confirm_text"),
        FieldPanel("mailing_unsubscribe_title"),
        FieldPanel("mailing_unsubscribe_text"),
        FieldPanel("mailing_unsubscribe_confirm_title"),
        FieldPanel("mailing_unsubscribe_confirm_text"),
        PageChooserPanel("mailing_confirm_page"),
        FieldPanel("mailing_confirm_button_text")
    ]

    edit_handler = TabbedInterface([
        ObjectList(generic_panels, heading=_("Generic settings")),
        ObjectList(mailing_panels, heading=_("Newsletter settings")),
    ])
    
    select_related = ["logo"]

    class Meta:
        verbose_name = _("Custom Site Settings")


