from django import template
from django.utils.safestring import SafeString
from wagtail.templatetags.wagtailcore_tags import richtext
from wagtail.rich_text import RichText
import bleach


register = template.Library()


ALLOWED_ATTRIBUTES = {
    "img": ["src", "alt"],
    "a": ["href"],
    "img": ["alt", "src", "class", "width", "height"],
    "iframe": ["width", "height", "src", "alt", "frameborder", "allow",
               "referrerpolicy", "title", "allowfullscreen"],
}
ALLOWED_ATTRIBUTES = bleach.ALLOWED_ATTRIBUTES | ALLOWED_ATTRIBUTES

ALLOWED_INLINE_TAGS = {"b", "i", "strong", "em", "br", "q", "a"}
ALLOWED_BLOCK_TAGS = ALLOWED_INLINE_TAGS |\
    {"img", "p", "h2", "h3", "h4", "ul", "ol", "div", "iframe", }


@register.filter
def inline_richtext(content: SafeString|RichText) -> RichText:
    rich_content = richtext(content)
    if isinstance(rich_content, RichText):
        rich_content = rich_content.source
    rich_content = RichText(
        bleach.clean(
            rich_content, strip=True,
            tags=ALLOWED_INLINE_TAGS,
            attributes=ALLOWED_ATTRIBUTES,
        ))
    
    return rich_content


@register.filter
def block_richtext(content: SafeString|RichText) -> SafeString|RichText:
    rich_content = richtext(content)
    rich_content = bleach.clean(
        str(rich_content), strip=True,
        tags=ALLOWED_BLOCK_TAGS,
        attributes=ALLOWED_ATTRIBUTES,
    )
    
    return RichText(rich_content)

