��    q      �  �   ,      �	     �	     �	     �	     �	  	   �	  
   �	     �	  
    
     
     
     #
     1
     8
     E
     K
     ^
     p
     �
     �
     �
     �
     �
  "   �
  	   �
  	   �
  	   �
  
                       %     3     9     M  	   Z     d     q     �  	   �     �     �     �     �     �     �     �     �  %         &     E  %   [  &   �     �  !   �  "   �               2     :     B     V  
   [     f     �     �     �  .   �  �   �     f     r  
     
   �  	   �  	   �     �     �     �     �     �     �          #     0     D     M     V     \     c     l     q     y     �     �     �     �     �     �     �          #     /     ;     M     R     p     v     �  
   �     �     �     �     �  �   �  "   �          %  0   *     [  	   d     n     �     �  	   �     �     �     �     �     �     �          '  $   0     U     Z     n  -   z     �  	   �     �     �  
   �     �     �     �       !   $     F     \     o  !   �     �     �  	   �     �     �               ,     :     A  N   F  8   �  ,   �  9   �  9   5  ,   o  :   �  :   �  '     '   :     b     u     }     �  
   �  !   �     �     �       3     �   ;     �     �            
   )  	   4     >     U  "   j     �     �     �     �     �     �     �            
     	   !     +     3     ;  (   M     v     �     �     �  6   �  &   �     
          %     1     G      M     n  %   t  *   �     �     �  0   �  /        ?     .             o       2   =               j   L   e   @   7   Y       ;   )   4   0       	   a          F   -   \   B   l   M   b           `   N       i          <   !   A       P   *   C       ^   Q   U         /          [   &           ]   '          G       8   K         >   3                  :   f              _       I   R      g       
   D   V   S   E      6   O   5   ?   X       c   h   Z   J   1   p           (   d              H   $       #   %   9                             W                     q   "       n   +          m            k      T   ,    %s has been added to your cart About Section All Allow order a specific quantity Attribute Attributes Back to homepage Base price Call to Action Carousel Carousel Item Client Clients list Color Confirm your email Confirmation text Contact Information Contacts Custom Site Settings Date Default color Description Do you really want to unsubscribe? FAQ block Fade-down Fade-left Fade-right Fade-up Favicon Feature Features list Float Footer Links Header Footer links Form Page Form Snippet From %(base_price)s€ Generic settings Home Page Hourly Image alternative text Image with alt text Inline Block Inline Item Integer Left Logo Mail confirmation landing button text Mail confirmation landing page Mail unsubscribe text Mail unsubscription confirmation text Mail unsubscription confirmation title Mail unsubscription title Mail validation confirmation text Mail validation confirmation title Mail validation text Mail validation title Members Monthly Multi choice option Name Newsletter Newsletter block description Newsletter block header Newsletter settings None Only display products that have this attribute Only show product that have this value for the given attribute. Set an empty value to show every product that have this attribute. Option name Option value Order page Order this Paragraph Portfolio Portfolio Item Price modifier Price modifier if set Pricing Block Pricing Option Product Attributes Product Category Page Product Page Product unique code Question Response Right Search See more Send Service Services List Show only children products Simple Page Single Single choice option Social Link Subscription confirmation text Subscription confirmation title Team Listing Team Member Testimonial Testimonials List Text Thanks for your subscription! Title User Value Choice User info form Value code Yearly Your email is now confirmed! Your email is now unsubscribed. alternative text Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-05-13 17:24+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4
 %s a été ajouté à votre panier Section À Propos Tous Permettre de commander une quantité spécifique Attribut Attributs Retour à l’accueil Prix de base Appel à action Carrousel Élément de carrousel Client Liste de clients Couleur Confirmez votre adresse Texte de confirmation Information de contact Contacts Configuration personnalisée de site Date Couleur par défaut Description Voulez-vous réellement vous désinscrire ? Bloc FAQ Fondu bas Fondu gauche Fondu droite Fondu haut Favicon Fonctionnalité Liste de fonctionnalités Nombre à virgule En-tête de liens de pied de page Liens de pied de page Page de formulaire Formulaire réutilisable À partir de %(base_price)s € Paramètres génériques Page d’accueil Par heure Texte alternatif Image avec texte alternatif Bloc en ligne Élément en ligne Nombre entier Gauche Logo Texte du bouton de la page d’aterrissage de la confirmation d’adresse mail Page d’aterrissage de la confirmation d’adresse mail Texte de désinscription de l’adresse mail Texte de confirmation de désinscription à la newsletter Titre de confirmation de désinscription à la newsletter Titre de désinscription de l’adresse mail Texte de confirmation de la validation de l’adresse mail Titre de confirmation de la validation de l’adresse mail Texte de validation de l’adresse mail Titre de validation de l’adresse mail Membre d’équipe Mensuel Option à choix multiples Nom Newsletter Description du bloc de newsletter En-tête du bloc de newsletter Paramètres de newsletter Aucun Afficher uniquement les produits ayant cet attribut Afficher uniquement les produits qui ont cette valeur pour l’attribut spécifié.Entrer une valeur vide pour montrer tous les produits qui possèdent l’attribut. Nom de l’option Valeur de l’option Page de commande Passer commande Paragraphe Portfolio Élément de portfolio Modificateur de prix Modificateur de produit si défini Bloc de prix Option de prix Attributs de produit Page catégorie de produit Page produit Code unique du produit Question Réponse Droite Rechercher Voir plus Envoyer Service Liste de services Afficher uniquement les produits enfants Page simple Unique Option à choix unique Lien social Texte de confirmation d’inscription à la newsletter Titre de confirmation à la newsletter Équipe Membre d’équipe Témoignage Liste de témoignages Texte Merci pour votre inscription ! Titre Valeur entrée par l’utilisateurice Formulaire d’informations utilisateurice Code de la valeur Annuel Votre adresse mail est maintenant confirmée ! Votre adresse mail est maintenant désinscrite. texte alternatif 