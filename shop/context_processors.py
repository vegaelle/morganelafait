from .models import Basket

def basket(request):
    basket, _ = Basket.objects.get_or_create_from_request(request)
    return {"basket": basket}

