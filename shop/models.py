from collections import defaultdict
from operator import or_
from functools import reduce
from django.core.paginator import Paginator
from django.db import models
from django.db.models import Q
from django import forms
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.contrib import messages
from django.shortcuts import redirect
from django_filters.fields import ChoiceIterator, ModelChoiceIterator, ModelMultipleChoiceField
from django_filters.widgets import RangeWidget
from modelcluster.fields import ParentalManyToManyField
from wagtail import blocks
from wagtail.admin.filters import django_filters
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField, StreamField
from coderedcms.models import ClassifierTerm, CoderedWebPage

from salesman.orders.models import (
    BaseOrder,
    BaseOrderItem,
    BaseOrderNote,
    BaseOrderPayment,
)
from salesman.basket.models import BaseBasket, BaseBasketItem
from wagtail.images.blocks import ImageChooserBlock

from .forms import DualSliderRangeWidget
from website import models as website
from website.blocks import (
    CtaBlock, ServicesBlock, PortfolioBlock, ClientsBlock, FeaturesBlock,
    TestimonialsBlock, AboutBlock, TeamBlock, PricingBlock, InlineBlock, ParagraphBlock,
    FAQBlock, NavbarLinkBlock, NavbarDropdownBlock, TextFooterColumnBlock,
    LinksFooterColumnBlock, SubscribeFormFooterColumnBlock, 
)


USER_VALUE_WIDGETS = (
    ("text", _("Text")),
    ("int", _("Integer")),
    ("float", _("Float")),
    ("date", _("Date")),
    ("color", _("Color")),
)


BOOLEAN_OPERATORS = (
    ("or", _("Or")),
    ("and", _("And"))
)


class CustomModelChoiceIterator(ModelChoiceIterator):

    def choice(self, obj):
        c = super().choice(obj)
        return (c[0], self.field.choice_label(obj))


class CustomModelMultipleChoiceField(ModelMultipleChoiceField):
    iterator = CustomModelChoiceIterator
    
    def __init__(self, *args, choice_label = str, **kwargs):
        self.choice_label = choice_label
        super().__init__(*args, **kwargs)
    

class CustomModelMultipleChoiceFilter(django_filters.ModelMultipleChoiceFilter):
    field_class = CustomModelMultipleChoiceField


class OptionValue(blocks.StructBlock):
    label = blocks.CharBlock(label=_("Option value"))
    code = blocks.CharBlock(label=_("Value code"))
    price_modifier = blocks.FloatBlock(label=_("Price modifier"), required=False)


class UserValueOption(blocks.StructBlock):
    label = blocks.CharBlock(label=_("Option name"))
    widget = blocks.ChoiceBlock(USER_VALUE_WIDGETS, default=USER_VALUE_WIDGETS[0][0]),
    code = blocks.CharBlock(label=_("Value code"))
    price_modifier = blocks.FloatBlock(label=_("Price modifier if set"), required=False)


class AbstractProductPage(CoderedWebPage):
    can_be_ordered = models.BooleanField(_("can be ordered"), default=True)
    short_description = RichTextField(_("short description"),
                                      features=["bold", "italic"])
    body = RichTextField()
    base_price = models.DecimalField(_("base price"), max_digits=6, decimal_places=2)
    computed_price = models.DecimalField(_("computed price"), default=0, max_digits=6,
                                         decimal_places=2)
    code = models.CharField(_("Product unique code"), unique=True, max_length=50)
    options = StreamField([
        ("single_choice", blocks.ListBlock(OptionValue, label=_("Single choice option"))),
        ("multi_choice", blocks.ListBlock(OptionValue, label=_("Multi choice option"))),
        ("user_value_choice", UserValueOption(label=_("User Value Choice"))),
    ], null=True, blank=True)
    allow_quantity = models.BooleanField(
        verbose_name=_("Allow order a specific quantity"), default=True)

    images = StreamField([
        ("image", ImageChooserBlock()),
    ])
    
    long_description = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("features", FeaturesBlock()),
        ("about", AboutBlock()),
        ("team", TeamBlock()),
        ("pricing", PricingBlock()),
        ("faq", FAQBlock()),
        ("inline", InlineBlock()),
        ("paragraph", ParagraphBlock()),
        
    ], blank=True)
    # attributes_title = models.CharField(max_length=50, blank=True)

    content_panels = CoderedWebPage.content_panels + [
        FieldPanel("title"),
        FieldPanel("can_be_ordered"),
        FieldPanel("cover_image"),
        FieldPanel("code"),
        FieldPanel("base_price"),
        FieldPanel("short_description"),
        # FieldPanel("body"),
        FieldPanel("images"),
        FieldPanel("options"),
    ]
    # body_content_panels = []

    @property
    def price(self):
        """
        Calculates the total price of the product, with the base price and options
        """ 
        computed_price = self.base_price
        if self.computed_price != computed_price:
            self.computed_price = computed_price
            self.save()
        return self.computed_price

    @property
    def first_image(self):
        try:
            return self.images[0].value
        except IndexError:
            pass 

    @cached_property
    def grouped_classifierterms(self):
        classifiers = defaultdict(list)
        for term in self.classifier_terms.all():
            classifiers[term.classifier].append(term.name)
        return classifiers.items()

    def serve(self, request, *args, **kwargs):
        # if request.method == 'POST' and 'add_to_cart' in request.POST:
        #     # add this to basket
        #     basket, _created = Basket.objects.get_or_create_from_request(request)
        #     basket.add(self)
        #     messages.success(request, _("%s has been added to your cart") % self)
        #     return redirect(request.get_full_path())
        return super().serve(request, *args, **kwargs)

 
    class Meta:
        verbose_name = _("Product Page")
        abstract = True


class ProductPage(AbstractProductPage):

    parent_page_types = ["shop.ProductLandingPage", "shop.ProductPage", ]
    subpage_types = ["shop.ProductPage", "website.FormPage", ]

    template = "shop/pages/product_page.html"
    search_template = "shop/pages/product_page.search.html"
    miniview_template = "shop/pages/product_page.mini.html"

    @property
    def form_page(self):
        return website.FormPage.objects.child_of(self).live().first()


class ProductLandingPage(CoderedWebPage):
    body = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("features", FeaturesBlock()),
        ("about", AboutBlock()),
        ("team", TeamBlock()),
        ("pricing", PricingBlock()),
        ("faq", FAQBlock()),
        ("inline", InlineBlock()),
        ("paragraph", ParagraphBlock()),
        
    ], blank=True)

    # product pages filtering
    root_page = models.ForeignKey(
        "wagtailcore.Page",
        verbose_name=_("root page for products"),
        on_delete=models.SET_NULL, null=True, blank=True,
        related_name="used_as_product_landing_root_by",
        help_text=_("only children of this page will be included. Leave blank for"
                    " current page"),
    )
    only_direct_children = models.BooleanField(_("only include direct children"),
                                               default=False)
    only_orderable_products = models.BooleanField(
        _("only include products that can be ordered"),
        default=False)
    children_classifierterms = ParentalManyToManyField(
        "coderedcms.ClassifierTerm",
        blank=True,
        verbose_name=_("products classifiers"),
        related_name="used_for_products_selection_by",
        help_text=_("only include products that have these classifiers."),
    )
    classifiers_boolean_operator = models.CharField(
        _("boolean operator for products classifiers"),
        max_length=3, choices=BOOLEAN_OPERATORS, default="or",
        help_text=_("choose if any of the specified classifiers should be present, "
                    "or only at least one of them"),
    )
    children_filter_classifiers = ParentalManyToManyField(
        "coderedcms.Classifier",
        blank=True,
        verbose_name=_("filterable classifiers for products"),
        related_name="used_as_products_filter_by",
        help_text=_("Enable filtering product pages by these classifiers."),
    )

    subpage_types = ["shop.ProductLandingPage", "shop.ProductPage", ]
    template = "shop/pages/product_landing_page.html"
    search_template = "shop/pages/product_landing_page.search.html"

    content_panels = CoderedWebPage.content_panels + [
        FieldPanel("root_page"),
        FieldPanel("only_direct_children"),
        FieldPanel("only_orderable_products"),
        FieldPanel("children_classifierterms"),
        FieldPanel("classifiers_boolean_operator"),
        FieldPanel("children_filter_classifiers"),
    ]

    def get_products(self):
        root_page = self.root_page or self
        qs = ProductPage.objects.live()
        if self.only_direct_children:        
            qs = qs.child_of(root_page)
        else:
            qs = qs.descendant_of(root_page)

        if self.only_orderable_products:
            qs = qs.filter(can_be_ordered=True)

        terms = self.children_classifierterms.all()
        if self.classifiers_boolean_operator == 'or':
            filters = []
            for term in terms:
                filters.append(Q(classifier_terms=term))
            if filters:
                qs = qs.filter(reduce(or_, filters))
        else:
            for term in terms:
                qs = qs.filter(classifier_terms=term)
        return qs

    def get_filter(self, request, qs):

        price_bounds = qs.aggregate(models.Min("computed_price"),
                                   models.Max("computed_price"))

        active_classifierterms = ClassifierTerm.objects.filter(coderedpage__in=qs)
        classifierterms = defaultdict(set)
        for term in active_classifierterms:
            classifierterms[term.classifier.id].add(term.id)

        class BaseProductFilter(django_filters.FilterSet):
            name = django_filters.filters.CharFilter(field_name="title",
                                                     label=_("Title"),
                                                     max_length=100,
                                                     lookup_expr="icontains")
            price = django_filters.filters.RangeFilter(
                field_name="base_price",
                label=_("Price"),
                widget=DualSliderRangeWidget(attrs={
                    "min": price_bounds["computed_price__min"],
                    "max": price_bounds["computed_price__max"],
                }),
            )

            class Meta:
                model = ProductPage
                fields = {}

        fields = {}

        for classifier in self.children_filter_classifiers.all():
            if len(classifierterms[classifier.id]) > 1:
                fields[classifier.slug] = CustomModelMultipleChoiceFilter(
                    label=classifier.name,
                    field_name="classifier_terms",
                    queryset=classifier.terms.filter(id__in=classifierterms[classifier.id]),
                    widget=forms.widgets.CheckboxSelectMultiple,
                    choice_label=lambda o: o.name,
                )
        
        ProductFilter = type("ProductFilter", (BaseProductFilter, ), fields)

        filter = ProductFilter(request.GET, qs)

        return filter

    def get_context(self, request, *args, **kwargs):
        ctx = super().get_context(request, *args, **kwargs)
        products_qs = self.get_products()
        filter = self.get_filter(request, products_qs)
        # TODO: set order customizable
        paginator = Paginator(filter.qs, 12)  # TODO: maybe set this customizable
        page = request.GET.get("p", 1)
        page_obj = paginator.get_page(page)
        
        ctx["products"] = page_obj
        ctx["paginator"] = paginator
        ctx["filters"] = filter.form
        return ctx        
 
    class Meta:
        verbose_name = _("Product Landing Page")


class Order(BaseOrder):
    pass


class OrderItem(BaseOrderItem):
    pass


class OrderPayment(BaseOrderPayment):
    pass


class OrderNote(BaseOrderNote):
    pass


class Basket(BaseBasket):
    pass


class BasketItem(BaseBasketItem):
    pass
