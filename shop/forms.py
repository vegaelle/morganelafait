from django import forms
from django_bootstrap5.renderers import FieldRenderer
from django_filters.fields import RangeWidget
from django_filters.widgets import SuffixedMultiWidget


class CustomFieldRenderer(FieldRenderer):

    def __init__(self, *args, **kwargs):
        self.ignore_server_validation = kwargs.pop("ignore_server_validation", False)

        super().__init__(*args, **kwargs)


    def get_server_side_validation_classes(self):
        if self.ignore_server_validation:
            return ""
        return super().get_server_side_validation_classes()


class DualSliderRangeWidget(RangeWidget):
    template_name = "shop/widgets/dual_slider.html"

    def __init__(self, attrs=None):
        if attrs:
            # attrs["type"] = "range"
            # attrs["class"] = "form-range"
            # attrs["step"] = 1
            widgets = (forms.TextInput({"value": attrs["min"], **attrs}),
                       forms.TextInput({"value": attrs["max"], **attrs}))
        else:
            widgets = (forms.TextInput, forms.TextInput)
        super(SuffixedMultiWidget, self).__init__(widgets, attrs)
