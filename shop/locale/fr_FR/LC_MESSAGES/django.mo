��    +      t  ;   �      �     �     �     �     �     �  4   �     !     (     0     6     >     R     W     c     p     s     |     �     �     �     �     �     �     �     �     �  
          
   '  )   2     \  Z   k     �  #   �  I   �     C  )   `  2   �     �     �  
   �     �  �          0        7     :     B  C   G     �     �     �     �     �     �     �     �     �     �     	     	     	     <	     V	     c	     z	     �	     �	     �	     �	     �	     �	  8   �	     
  s   0
     �
  ,   �
  Y   �
  &   9  ;   `  ;   �     �     �  	             '         (      &   #   %                     
                               "         !   *                             $                            )             +                      	               All Allow order a specific quantity And Color Date Enable filtering product pages by these classifiers. Filter Filters Float Integer Multi choice option Next Option name Option value Or Previous Price Price modifier Price modifier if set Product Landing Page Product Page Product unique code Single choice option Text Title User Value Choice Value code View product base price boolean operator for products classifiers can be ordered choose if any of the specified classifiers should be present, or only at least one of them computed price filterable classifiers for products only children of this page will be included. Leave blank for current page only include direct children only include products that can be ordered only include products that have these classifiers. products classifiers root page for products shopOrder short description Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-09-06 19:06+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
 Tous Permettre de commander une quantité spécifique Et Couleur Date Permettre le filtrage des pages produits selon ces classificateurs. Filtrer Filtres Flottant Entier Option à choix multiple Suivant Nom de l’option Valeur de l’option Ou Précédent Prix Modificateur de prix Modificateur de prix si défini Page de liste de Produits Page Produit Code unique du produit Option à choix unique Texte Titre Option à choix libre Code de la valeur Voir le produit prix de base opérateur booléen pour les classificateurs de produits peut être commandé choisir si tous les classificateurs spécifiés doivent être présents, ou seulement au moins l’un d’entre eux prix calculé classificateurs filtrables pour les produits seuls les enfants de cette page seront inclus. Laisser vide pour prendre la page courante inclure uniquement les enfants directs seulement inclure les produits qui peuvent être commandés seulement inclure les produits qui ont ces classificateurs. classificateurs de produits page racine pour les produits Commander description courte 