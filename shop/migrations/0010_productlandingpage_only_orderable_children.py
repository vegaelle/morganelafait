# Generated by Django 4.2.13 on 2024-09-04 12:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0009_productpage_can_be_ordered'),
    ]

    operations = [
        migrations.AddField(
            model_name='productlandingpage',
            name='only_orderable_children',
            field=models.BooleanField(default=False, verbose_name='only include products that can be ordered'),
        ),
    ]
