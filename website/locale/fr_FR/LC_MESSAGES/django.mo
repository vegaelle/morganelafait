��    >        S   �      H     I  	   W     a     h     w     �     �     �     �     �     �     �     �     �  	   �  	     	     
              (     0  	   >     H     O     \     h     m     z     �     �     �     �  	   �  	   �     �     �     �     �     �     �     �     �     �                    !     (     4     ;     G     `     m     y     �     �     �     �     �     �     �  �   �     �  	   �     �     �  	   	     	     $	     +	     <	     C	  !   T	     v	     �	     �	     �	  	   �	     �	     �	  
   �	     �	     �	     
     
     
     *
     =
     D
     U
     ]
     e
     i
     q
  
   w
  	   �
     �
     �
     �
     �
     �
     �
     �
  	   �
  
   �
  	   �
     �
     �
                     '  %   3     Y     m     �     �     �     �     �     �     �     �     #                   <   ;            2      %              >   !   7   4   ,                     "   1           =   
   .             &                9   )       0   -          +              /         *         	   5   3           6         '             8       $                         (   :    About Section Animation Border Call to Action Carousel Carousel Item Cart Center text Client Clients list Column relative size (1-12) Contact Information Content Description FAQ block Fade-down Fade-left Fade-right Fade-up Feature Features list Home Page Hourly Inline Block Inline Item Left Links column Members Monthly Name Next None Paragraph Portfolio Previous Pricing Block Pricing Option Question Related Response Right Search Search  See more Service Services List Shadow Simple Page Single Social Link Subscription Form column Team Listing Team Member Testimonial Testimonials List Text Text column Title Yearly Your account {user}’s avatar Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-08-27 12:23+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
 Section À Propos Animation Bordure Appel à action Carrousel Élément de carrousel Panier Centrer le texte Client Liste de clients Taille relative de colonne (1-12) Information de contact Contenu Description Bloc FAQ Fondu bas Fondu gauche Fondu droite Fondu haut Fonctionnalité Liste de fonctionnalités Page d’accueil Horaire Bloc en ligne Élément en ligne Gauche Colonne de liens Membres Mensuel Nom Suivant Aucun Paragraphe Portfolio Précédent Bloc de prix Option de prix Question En lien Réponse Droite Recherche Rechercher Voir plus Service Liste de services Ombre Page simple Unique Lien social Colonne de formulaire d’inscription Listing d’équipe Membre d’équipe Témoignage Liste de témoignages Texte Colonne de texte Titre Annuel Votre compte Avatar de {user} 