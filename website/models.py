"""
Create or customize your page models here.
"""

from django.db import models
from django.utils.translation import gettext as _
from wagtail import blocks
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField, StreamField
from wagtail.snippets.models import register_snippet
from modelcluster.fields import ParentalKey
from coderedcms.models import CoderedArticleIndexPage, CoderedPage
from coderedcms.models import CoderedArticlePage
from coderedcms.models import CoderedEmail
from coderedcms.models import CoderedStreamFormPage
from coderedcms.models import CoderedWebPage
    

from .blocks import (
    CtaBlock, ServicesBlock, PortfolioBlock, ClientsBlock, FeaturesBlock,
    TestimonialsBlock, AboutBlock, TeamBlock, PricingBlock, InlineBlock, ParagraphBlock,
    FAQBlock, NavbarLinkBlock, NavbarDropdownBlock, TextFooterColumnBlock,
    LinksFooterColumnBlock, SubscribeFormFooterColumnBlock, 
)


class HomePage(CoderedWebPage):
    hero_title = models.CharField(_("Hero section title"), max_length=100)
    hero_content = RichTextField(_("Hero section content"))
    hero_page = ParentalKey("wagtailcore.Page", related_name="used_as_hero_link_by",
                            on_delete=models.SET_NULL, null=True, blank=True)
    hero_page_button = models.CharField(_("Hero section button text"), max_length=100,
                                        default=_("See more"))
    body = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("paragraph", ParagraphBlock()),
    ], blank=True)

    content_panels = [
        FieldPanel("hero_title"),
        FieldPanel("hero_content"),
        FieldPanel("hero_page"),
        FieldPanel("hero_page_button"),
    ] + CoderedWebPage.content_panels
    parent_page_types = ["wagtailcore.Page"]

    class Meta:
        verbose_name = _("Home Page")
        

class SimplePage(CoderedWebPage):
    body = StreamField([
        ("cta", CtaBlock()),
        ("services", ServicesBlock()),
        ("portfolio", PortfolioBlock()),
        ("clients", ClientsBlock()),
        ("testimonials", TestimonialsBlock()),
        ("features", FeaturesBlock()),
        ("about", AboutBlock()),
        ("team", TeamBlock()),
        ("pricing", PricingBlock()),
        ("faq", FAQBlock()),
        ("inline", InlineBlock()),
        ("paragraph", ParagraphBlock()),
        
    ], blank=True)
 
    class Meta:
        verbose_name = _("Simple Page")


class ArticlePage(CoderedArticlePage):
    """
    Article, suitable for news or blog content.
    """

    class Meta:
        verbose_name = "Article"
        ordering = ["-first_published_at"]

    # Only allow this page to be created beneath an ArticleIndexPage.
    parent_page_types = ["website.ArticleIndexPage"]

    template = "coderedcms/pages/article_page.html"
    search_template = "coderedcms/pages/article_page.search.html"


class ArticleIndexPage(CoderedArticleIndexPage):
    """
    Shows a list of article sub-pages.
    """

    class Meta:
        verbose_name = "Article Landing Page"

    # Override to specify custom index ordering choice/default.
    index_query_pagemodel = "website.ArticlePage"

    # Only allow ArticlePages beneath this page.
    subpage_types = ["website.ArticlePage"]

    template = "coderedcms/pages/article_index_page.html"


# class EventPage(CoderedEventPage):
#     class Meta:
#         verbose_name = "Event Page"

#     parent_page_types = ["website.EventIndexPage"]
#     template = "coderedcms/pages/event_page.html"


# class EventIndexPage(CoderedEventIndexPage):
#     """
#     Shows a list of event sub-pages.
#     """

#     class Meta:
#         verbose_name = "Events Landing Page"

#     index_query_pagemodel = "website.EventPage"

#     # Only allow EventPages beneath this page.
#     subpage_types = ["website.EventPage"]

#     template = "coderedcms/pages/event_index_page.html"


# class EventOccurrence(CoderedEventOccurrence):
#     event = ParentalKey(EventPage, related_name="occurrences")


class FormPage(CoderedStreamFormPage):
    """
    A page with an html <form>.
    """

    class Meta:
        verbose_name = "Form"

    template = "coderedcms/pages/stream_form_page.html"


# class FormPageField(CoderedFormField):
#     """
#     A field that links to a FormPage.
#     """

#     class Meta:
#         ordering = ["sort_order"]

#     page = ParentalKey("FormPage", related_name="form_fields")


class FormConfirmEmail(CoderedEmail):
    """
    Sends a confirmation email after submitting a FormPage.
    """

    page = ParentalKey("FormPage", related_name="confirmation_emails")


# class LocationPage(CoderedLocationPage):
#     """
#     A page that holds a location.  This could be a store, a restaurant, etc.
#     """

#     class Meta:
#         verbose_name = "Location Page"

#     template = "coderedcms/pages/location_page.html"

#     # Only allow LocationIndexPages above this page.
#     parent_page_types = ["website.LocationIndexPage"]


# class LocationIndexPage(CoderedLocationIndexPage):
#     """
#     A page that holds a list of locations and displays them with a Google Map.
#     This does require a Google Maps API Key in Settings > CRX Settings
#     """

#     class Meta:
#         verbose_name = "Location Landing Page"

#     # Override to specify custom index ordering choice/default.
#     index_query_pagemodel = "website.LocationPage"

#     # Only allow LocationPages beneath this page.
#     subpage_types = ["website.LocationPage"]

#     template = "coderedcms/pages/location_index_page.html"


# class WebPage(CoderedWebPage):
#     """
#     General use page with featureful streamfield and SEO attributes.
#     """

#     class Meta:
#         verbose_name = "Web Page"

#     template = "coderedcms/pages/web_page.html"


# -- Navbar & Footer ----------------------------------------------------------


@register_snippet
class Navbar(models.Model):
    """
    Custom navigation bar / menu.
    """

    class Meta:
        verbose_name = "Navigation Bar"

    name = models.CharField(
        max_length=255,
    )
    content = StreamField(
        [
            ("link", NavbarLinkBlock()),
            ("dropdown", NavbarDropdownBlock()),
            ("search", blocks.StructBlock(
                template="website/snippets/navbar_search.html", icon="search")),
            ("cart", blocks.StructBlock(
                template="website/snippets/navbar_cart.html", icon="table")),
            ("user", blocks.StructBlock(
                template="website/snippets/navbar_user.html", icon="user")),
        ],
        use_json_field=True,
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("content"),
    ]

    def __str__(self) -> str:
        return self.name


@register_snippet
class Footer(models.Model):
    """
    Custom footer for bottom of pages on the site.
    """

    class Meta:
        verbose_name = "Footer"

    name = models.CharField(
        max_length=255,
    )
    content = StreamField([
        ("text", TextFooterColumnBlock()),
        ("links", LinksFooterColumnBlock()),
        ("subscribe_form", SubscribeFormFooterColumnBlock()),
    ], verbose_name="Content", blank=True, use_json_field=True,
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("content"),
    ]

    def __str__(self) -> str:
        return self.name
