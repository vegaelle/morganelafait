from django import template

register = template.Library()

@register.filter
def display_choice(struct, choice_field_name: str):
    children_blocks = dict(struct.block.deconstruct()[1][0])
    choice_block = children_blocks[choice_field_name]
    choices = dict(choice_block.deconstruct()[2]["choices"])
    return choices.get(struct[choice_field_name], struct[choice_field_name])
