from django import template
from django.utils.safestring import SafeString
import bleach

from wagtail.templatetags.wagtailcore_tags import richtext
from wagtail.rich_text import RichText

from website.models import Footer
from website.models import Navbar


ALLOWED_ATTRIBUTES = {
    "img": ["src", "alt"],
    "a": ["href"],
    "img": ["alt", "src", "class", "width", "height"],
    "iframe": ["width", "height", "src", "alt", "frameborder", "allow",
               "referrerpolicy", "title", "allowfullscreen"],
}
ALLOWED_ATTRIBUTES = bleach.ALLOWED_ATTRIBUTES | ALLOWED_ATTRIBUTES

ALLOWED_INLINE_TAGS = {"b", "i", "strong", "em", "br", "q", "a"}
ALLOWED_BLOCK_TAGS = ALLOWED_INLINE_TAGS |\
    {"img", "p", "h2", "h3", "h4", "ul", "ol", "div", "iframe", }
ALLOWED_PROTOCOLS = bleach.ALLOWED_PROTOCOLS | {"tel"}


register = template.Library()


@register.simple_tag
def get_website_navbars():
    # NOTE: For a multi-site, you may need to create SiteSettings to
    # choose a Navbar, then query those here. Or, add a Foreign Key to
    # the Site on the Navbar, and query those.
    return Navbar.objects.all()


@register.simple_tag
def get_website_footers():
    # NOTE: For a multi-site, you may need to create SiteSettings to
    # choose a Footer, then query those here. Or, add a Foreign Key to
    # the Site on the Footer, and query those.
    return Footer.objects.all()

@register.filter
def inline_richtext(content: SafeString|RichText) -> RichText:
    rich_content = richtext(content)
    if isinstance(rich_content, RichText):
        rich_content = rich_content.source
    rich_content = RichText(
        bleach.clean(
            rich_content, strip=True,
            tags=ALLOWED_INLINE_TAGS,
            attributes=ALLOWED_ATTRIBUTES,
            protocols=ALLOWED_PROTOCOLS,
        ))
    
    return rich_content


@register.filter
def block_richtext(content: SafeString|RichText) -> SafeString|RichText:
    rich_content = richtext(content)
    rich_content = bleach.clean(
        str(rich_content), strip=True,
        tags=ALLOWED_BLOCK_TAGS,
        attributes=ALLOWED_ATTRIBUTES,
        protocols=ALLOWED_PROTOCOLS,
    )
    
    return RichText(rich_content)

