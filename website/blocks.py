from django.utils.translation import gettext as _

from wagtail.blocks import (
    PageChooserBlock, StructBlock, CharBlock, RichTextBlock, ListBlock, BooleanBlock,
    ChoiceBlock, URLBlock, FloatBlock, IntegerBlock, StreamBlock
)
from wagtail.images.blocks import ImageChooserBlock

from coderedcms.blocks import HTML_STREAMBLOCKS
from coderedcms.blocks import LAYOUT_STREAMBLOCKS
from coderedcms.blocks import BaseBlock
from coderedcms.blocks import BaseLinkBlock
from coderedcms.blocks import LinkStructValue

from mailing.forms import SubscriptionForm


EFFECTS = [
    ("none", _("None")),
    ("fade-up", _("Fade-up")),
    ("fade-down", _("Fade-down")),
    ("fade-left", _("Fade-left")),
    ("fade-right", _("Fade-right")),
]


class NavbarLinkBlock(BaseLinkBlock):
    """
    Simple link in the navbar.
    """
    use_in_mails = BooleanBlock(label=_("use in emails"), required=False, default=True)

    class Meta:
        icon = "link"
        label = "Link"
        template = "website/blocks/navbar_link.html"
        value_class = LinkStructValue


class NavbarDropdownBlock(BaseBlock):
    """
    Custom dropdown menu with heading, links, and rich content.
    """
    title = CharBlock(
        max_length=255,
        required=True,
        label="Title",
    )
    links = StreamBlock(
        [("link", NavbarLinkBlock())],
        required=True,
        label="Links",
    )
    description = StreamBlock(
        HTML_STREAMBLOCKS,
        required=False,
        label="Description",
    )
    use_in_mails = BooleanBlock(label=_("use in emails"), required=False, default=True)

    class Meta:
        icon = "arrow-down"
        label = "Dropdown"
        template = "website/blocks/navbar_dropdown.html"


class SocialLinkBlock(StructBlock):
    name = CharBlock(label=_("Name"))
    url = URLBlock()
    icon_name = CharBlock()
    show_in_header = BooleanBlock(default=True)

    class Meta:
        label = _("Social Link")


class ContactBlock(StructBlock):
    text = CharBlock(label=_("Text"))
    address = CharBlock(blank=True)
    icon_name = CharBlock()

    class Meta:
        label = _("Contact Information")


class FooterColumnBlock(StructBlock):
    col_size = IntegerBlock(label=_("Column relative size (1-12)"), required=False)
    title = CharBlock(label=_("Title"))


class TextFooterColumnBlock(FooterColumnBlock):
    content = RichTextBlock(label=_("Content"))

    class Meta:
        label = _("Text column")
        template = "website/blocks/footer/text.html"
        icon = "edit"


class LinksFooterColumnBlock(FooterColumnBlock):
    links = ListBlock(NavbarLinkBlock(), label=_("Content"))

    class Meta:
        label = _("Links column")
        template = "website/blocks/footer/links.html"
        icon = "list-ul"


class SubscribeFormFooterColumnBlock(FooterColumnBlock):
    title = CharBlock(label=_("Title"))
    description = RichTextBlock(label=_("Description"), blank=True, default="")

    def get_context(self, value, parent_context):
        ctx = super().get_context(value, parent_context)
        form = SubscriptionForm()
        ctx["form"] = form
        return ctx

    class Meta:
        label = _("Subscription Form column")
        template = "website/blocks/footer/subscribe_form.html"
        icon = "cr-newspaper-o"


# page content blocks

class HeroItemBlock(StructBlock):
    background_image = ImageChooserBlock()
    title = RichTextBlock(label=_("Title"), features=["bold", "italic", "link"])
    text = RichTextBlock(label=_("Text"))
    page = PageChooserBlock(required=False)
    button_text = CharBlock(default=_("See more"))

    class Meta:
        label = _("Carousel Item")
    

class HeroBlock(StructBlock):
    items = ListBlock(HeroItemBlock)

    class Meta:
        template = "website/blocks/hero.html"
        label = _("Carousel")
        icon = "desktop"


class CtaBlock(StructBlock):
    title = RichTextBlock(label=_("Title"), features=["bold", "italic", "link"])
    text = RichTextBlock(label=_("Text"))
    page = PageChooserBlock()
    button_text = CharBlock()
    alternate_layout = BooleanBlock(default=False, required=False)

    class Meta:
        template = "website/blocks/cta.html"
        label = _("Call to Action")
        icon = "arrow-right-full"


class ServiceItemBlock(StructBlock):
    title = CharBlock(label=_("Title"))
    icon_name = CharBlock()
    text = RichTextBlock(label=_("Text"))
    page = PageChooserBlock()

    class Meta:
        label = _("Service")

class ServicesBlock(StructBlock):
    items = ListBlock(ServiceItemBlock())

    class Meta:
        template = "website/blocks/services.html"
        label = _("Services List")
        icon = "draft"
    

class TitledBlock(StructBlock):
    title = RichTextBlock(label=_("Title"), features=["bold", "italic", "link"])
    text = RichTextBlock(label=_("Text"), required=False)


class PortfolioBlock(TitledBlock):
    items = ListBlock(PageChooserBlock(page_type="shop.ProductPage"))

    class Meta:
        template = "website/blocks/portfolio.html"
        label = _("Portfolio")
        icon = "image"


class ClientItemBlock(StructBlock):
    name = CharBlock(label=_("Name"))
    image = ImageChooserBlock()

    class Meta:
        template = "website/blocks/client_item.html"
        label = _("Client")


class ClientsBlock(TitledBlock):
    clients = ListBlock(ClientItemBlock())

    class Meta:
        template = "website/blocks/clients.html"
        label = _("Clients list")
        icon = "user"


class FeatureItemBlock(StructBlock):
    title = RichTextBlock(label=_("Title"), features=["bold", "italic", "link"])
    content = RichTextBlock(label=_("Content"))

    class Meta:
        label = _("Feature")


class FeaturesBlock(TitledBlock):
    items = ListBlock(FeatureItemBlock())

    class Meta:
        template = "website/blocks/features.html"
        label = _("Features list")
        icon = "cogs"


class AboutBlock(TitledBlock):
    items = ListBlock(StructBlock([
        ("title", RichTextBlock(features=["bold", "italic", "link"])),
        ("text", RichTextBlock()),
        ("icon_name", CharBlock()),
    ]))
    image = ImageChooserBlock()
    image_position = ChoiceBlock([
        ("left", _("Left")),
        ("right", _("Right")),
    ], default="left")

    class Meta:
        template = "website/blocks/about.html"
        label = _("About Section")
        icon = "comment"


class TeamItemBlock(StructBlock):
    name = CharBlock(label=_("Name"))
    description = RichTextBlock(label=_("Description"))
    image = ImageChooserBlock()
    social_links = ListBlock(SocialLinkBlock())

    class Meta:
        template = "website/blocks/team_item.html"
        label = _("Team Member")


class TeamBlock(TitledBlock):
    items = ListBlock(TeamItemBlock(), label=_("Members"))

    class Meta:
        template = "website/blocks/team.html"
        label = _("Team Listing")
        icon = "group"


class TestimonialItemBlock(StructBlock):
    name = CharBlock(label=_("Name"))
    description = RichTextBlock(label=_("Description"))
    text = RichTextBlock(label=_("Text"))
    image = ImageChooserBlock()

    class Meta:
        label = _("Testimonial")


class TestimonialsBlock(TitledBlock):
    items = ListBlock(TestimonialItemBlock())

    class Meta:
        template = "website/blocks/testimonial.html"
        label = _("Testimonials List")
        icon = "openquote"


class PricingItemBlock(StructBlock):
    name = CharBlock(label=_("Name"))
    price = StructBlock([
        ("amount", FloatBlock()),
        ("type", ChoiceBlock([
            ("single", _("Single")),
            ("hour", _("Hourly")),
            ("month", _("Monthly")),
            ("year", _("Yearly")),
            ])
         )
    ])
    items = ListBlock(StructBlock([
        ("label", CharBlock()),
        ("active", BooleanBlock(default=True)),
    ]))
    promoted = BooleanBlock(default=False, required=False)
    effect = ChoiceBlock(EFFECTS, default="none")
    page = PageChooserBlock()
    button_text = CharBlock()
    
    class Meta:
        template = "website/blocks/pricing_item.html"
        label = _("Pricing Option")


class PricingBlock(TitledBlock):
    items = ListBlock(PricingItemBlock())

    class Meta:
        template = "website/blocks/pricing.html"
        label = _("Pricing Block")
        icon = "decimal"


class InlineItemBlock(StructBlock):
    title = CharBlock(label=_("Title"))
    text = RichTextBlock(label=_("Text"))
    icon_name = CharBlock()

    class Meta:
        label = _("Inline Item")


class InlineBlock(StructBlock):
    items = ListBlock(InlineItemBlock())

    class Meta:
        template = "website/blocks/inline.html"
        label = _("Inline Block")
        icon = "dots-horizontal"


class ParagraphBlock(StructBlock):
    title = CharBlock(label=_("Title"))
    text = RichTextBlock(label=_("Text"))
    center = BooleanBlock(label=_("Center text"), default=True, required=False)
    effect = ChoiceBlock(label=_("Animation"), choices=EFFECTS, default="none")
    border = BooleanBlock(label=_("Border"), default=False, required=False)
    shadow = BooleanBlock(label=_("Shadow"), default=False, required=False)

    class Meta:
        template = "website/blocks/paragraph.html"
        label = _("Paragraph")
        icon = "pilcrow"


class FAQBlock(TitledBlock):
    items = ListBlock(StructBlock([
        ("question", CharBlock(label=_("Question"))),
        ("response", RichTextBlock(label=_("Response"))),
    ]))

    class Meta:
        template = "website/blocks/faq.html"
        label = _("FAQ block")
        icon = "help"
