��          �      �       0  )   1     [     l     r     �     �     �     �     �  9   �  #        &  �   4  /   /     _     u     |     �     �     �     �  
   �  \   �  @   :     {           
                              	                 A user with that username already exists. Back to homepage Error Important dates Internal server error Page not found Permissions Personal info Search Sorry, there seems to be an error. Please try again soon. Sorry, this page could not be found email address Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-08-27 12:24+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
 Un·e utilisateurice avec ce nom existe déjà. Retour à l’accueil Erreur Dates importantes Erreur interne Page introuvable Permissions Information personnelle Rechercher Nous sommes désolé·es, mais une erreur est survenue. Veuillez réessayer ultérieurement. Nous sommes désolé·es, mais cette page ne peut être trouvée adresse e-mail 