from django.apps import AppConfig


class SearchConfig(AppConfig):
    name = "search"
    label = "site_search"
