from django.utils.translation import gettext as _
from wagtail.blocks import (
    StructBlock, CharBlock, PageChooserBlock, ListBlock, IntegerBlock, BooleanBlock,
    ChoiceBlock, RichTextBlock
)
from wagtail.images.blocks import ImageChooserBlock
from birdsong.blocks import UnwrappedStreamBlock

# from shop.models import ProductPage

ALIGNMENTS = (
    ("none", _("None")),
    ("left", _("Left")),
    ("right", _("Right")),
    ("center", _("Center")),
    ("justify", _("Justified")),
)


class MailTextBlock(StructBlock):
    
    items = ListBlock(StructBlock(( 
        ("text", CharBlock(label=_("Text"))),
        ("size", IntegerBlock(label=_("Text size"), required=False)),
        ("colored", BooleanBlock(label=_("Colored text"), default=False,
                                 required=False)),
        ("alignment", ChoiceBlock(ALIGNMENTS, label=_("Alignment"), default="none"))
    )))

    class Meta:
        template = "mailing/blocks/mail_text.html"


class MailBlocks(UnwrappedStreamBlock):

    rich_text = RichTextBlock(
        template="mailing/blocks/richtext.html",
        features=["h3", "h4", "bold", "italic", "link", "ul", "ol", "document-link"],
    )
    hero = StructBlock([
        ("title", CharBlock(label=_("Title"))),
        ("text", MailTextBlock(label=_("Text"))),
        ("text_link", CharBlock(label=_("Text Link"))),
        ("page", PageChooserBlock(label=_("Page"))),
        ("background_image", ImageChooserBlock(label=_("Background Image"))),
    ], template="mailing/blocks/hero.html")
    carousel = ListBlock(StructBlock([
            ("image", ImageChooserBlock()),
            ("page", PageChooserBlock()),
   ]), template="mailing/blocks/carousel.html", icon="desktop")
    cta = StructBlock([
        ("icon", ImageChooserBlock(label=_("Icon"))),
        ("text", MailTextBlock(label=_("Text"))),
        ("button_text", CharBlock(label=_("Button text"), required=False)),
        ("page", PageChooserBlock(label=_("Page"), required=False)),
    ], template="mailing/blocks/cta.html", icon="arrow-right-full")
    products = StructBlock([
        ("title", MailTextBlock(label=_("Title"), required=False)),
        ("columns", IntegerBlock(label=_("Columns count"), default=3)),
        ("button_text", CharBlock(label=_("Button text"))),
        ("products", ListBlock(PageChooserBlock("shop.ProductPage", label=_("Products")))),
    ], template="mailing/blocks/products.html", icon="doc-full")
    showcase = ListBlock(StructBlock([
        ("title", MailTextBlock(label=_("Title"), required=False)),
        ("button_text", CharBlock(label=_("Button text"))),
        ("page", PageChooserBlock(label=_("Page"))),
        ("image", ImageChooserBlock(label=_("Image"))),
    ]), template="mailing/blocks/showcase.html", icon="image")
    infos_columns = ListBlock(StructBlock([
        ("title", CharBlock(label=_("Title"))),
        ("text", CharBlock(label=_("Text"))),
        ("icon", ImageChooserBlock(label=_("Icon"))),
    ]), template="mailing/blocks/info_columns.html", icon="dots-horizontal")

