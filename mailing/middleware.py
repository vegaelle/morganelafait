from django.shortcuts import redirect

from wagtail.models import Site

from .forms import SubscriptionForm

class MailingSubscriptionMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response
        
    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.method == "POST" and "newsletter_subscription_email" in request.POST:
            form = SubscriptionForm(request.POST)
            if form.is_valid():
                default_site = Site.objects.get(is_default_site=True)
                request.site = default_site
                form.save(request)
                return redirect("mailing_subscription_success")
        return view_func(request, *view_args, **view_kwargs)
