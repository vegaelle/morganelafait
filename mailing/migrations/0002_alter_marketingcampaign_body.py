# Generated by Django 5.0.3 on 2024-04-13 10:18

import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("mailing", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="marketingcampaign",
            name="body",
            field=wagtail.fields.StreamField(
                [
                    (
                        "rich_text",
                        wagtail.blocks.RichTextBlock(
                            features=[
                                "h3",
                                "h4",
                                "bold",
                                "italic",
                                "link",
                                "ul",
                                "ol",
                                "document-link",
                            ],
                            template="birdsong/mail/blocks/richtext.html",
                        ),
                    ),
                    (
                        "hero",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titre")),
                                (
                                    "text_link",
                                    wagtail.blocks.CharBlock(label="Text Link"),
                                ),
                                ("page", wagtail.blocks.PageChooserBlock(label="Page")),
                                (
                                    "background_image",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Background Image"
                                    ),
                                ),
                            ],
                            template="mailing/blocks/hero.html",
                        ),
                    ),
                    (
                        "carousel",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "images",
                                    wagtail.blocks.ListBlock(
                                        wagtail.images.blocks.ImageChooserBlock()
                                    ),
                                )
                            ],
                            icon="desktop",
                            template="mailing/blocks/carousel.html",
                        ),
                    ),
                    (
                        "cta",
                        wagtail.blocks.StructBlock(
                            [],
                            icon="arrow-right-full",
                            template="mailing/blocks/cta.html",
                        ),
                    ),
                ]
            ),
        ),
    ]
