# Generated by Django 5.0.3 on 2024-04-12 15:15

import django.db.models.deletion
import wagtail.blocks
import wagtail.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("birdsong", "0008_translation_support"),
    ]

    operations = [
        migrations.CreateModel(
            name="MarketingCampaign",
            fields=[
                (
                    "campaign_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="birdsong.campaign",
                    ),
                ),
                (
                    "body",
                    wagtail.fields.StreamField(
                        [
                            (
                                "rich_text",
                                wagtail.blocks.RichTextBlock(
                                    features=[
                                        "h3",
                                        "h4",
                                        "bold",
                                        "italic",
                                        "link",
                                        "ul",
                                        "ol",
                                        "document-link",
                                    ],
                                    template="birdsong/mail/blocks/richtext.html",
                                ),
                            )
                        ]
                    ),
                ),
            ],
            bases=("birdsong.campaign",),
        ),
    ]
