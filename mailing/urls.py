from django.urls import path

from . import views


urlpatterns = [
    path("inscription", views.subscription_confirm,
         name="mailing_subscription_success"),
    path("validation/<uuid:user_id>", views.validation,
         name="mailing_validation"),
    path("validation/<uuid:user_id>/confirmation", views.validation_confirm,
         name="mailing_validation_confirm"),
    path("desinscription/<uuid:user_id>", views.unsubscribe_user,
         name="mailing_unsubscribe"),
    path("desinscription/confirmation", views.unsubscribe_user_confirm,
         name="mailing_unsubscribe_confirm"),
    path("archive/<uuid:campaign_uuid>", views.campaign_view, name="campaign_view"),
]
