from django.contrib import messages
from .forms import SubscriptionForm


def mailing_form(request):
    if request.method == "POST" and "email" in request.POST:
        form = SubscriptionForm(request.POST)
        form.is_valid()
    else:
        form = SubscriptionForm()
    return {"subscription_form": form}
