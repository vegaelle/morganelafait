from django import forms
from django.db import IntegrityError
from django.utils.translation import gettext as _

from .models import MarketingContact


class SubscriptionForm(forms.Form):

    newsletter_subscription_email = forms.EmailField(label=_("Email address"))

    def save(self, request):
        existing_contact = MarketingContact.objects.filter(
            email=self.cleaned_data["newsletter_subscription_email"]
        )
        if len(existing_contact):  # birdsong doesn't have a unique constraint to email
            return existing_contact
        contact = MarketingContact(
            email=self.cleaned_data["newsletter_subscription_email"],
            validated=False)
        contact.save()
        contact.send_confirmation_mail(request)
        return contact
