import uuid
from birdsong.models import CampaignStatus
from django.http import HttpResponseRedirect
from django.utils.translation import gettext as _, pgettext

from birdsong.wagtail_hooks import (
    CampaignAdmin, ContactAdmin, BirdsongAdminGroup, modeladmin_re_register
)
from .models import MarketingCampaign, MarketingContact
from .filters import ContactFilter


class MyCampaignAdmin(CampaignAdmin):
    model = MarketingCampaign
    campaign = MarketingCampaign
    contact_class = MarketingContact
    contact_filter_class = ContactFilter

    def get_contacts_send_to(self, request):
        qs = super().get_contacts_send_to(request)
        qs = qs.filter(validated=True)
        return qs

    def copy(self, request, instance_pk):
        instance = self.model.objects.get(pk=instance_pk)
        instance.name = "{} ({})".format(instance.name, pgettext("noun", "Copy"))
        instance.pk = None
        instance.id = None
        instance.uuid = uuid.uuid4()
        instance.sent_date = None
        instance.status = CampaignStatus.UNSENT
        instance.save()
        return HttpResponseRedirect(self.url_helper.get_action_url("index"))

class MyContactAdmin(ContactAdmin):
    model = MarketingContact
    list_display = ("email", "validated", "registration_date")


@modeladmin_re_register
class BirdsongAdminGroup(BirdsongAdminGroup):
    # menu_item_name = "mailing"
    menu_label = _("Mailing")
    menu_icon = "mail"
    items = (MyCampaignAdmin, MyContactAdmin)
