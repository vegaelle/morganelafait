from django_filters import FilterSet
from django_filters.filters import (
    DateFromToRangeFilter, ModelMultipleChoiceFilter
)
from django_filters.widgets import DateRangeWidget
from taggit.models import Tag

from .models import MarketingContact


class ContactFilter(FilterSet):
    registration_date = DateFromToRangeFilter(
        widget=DateRangeWidget(attrs={"type": "date"}),
    )
    tags = ModelMultipleChoiceFilter(
        queryset=Tag.objects.all()
    )

    class Meta:
        model = MarketingContact
        fields = ["tags",]
