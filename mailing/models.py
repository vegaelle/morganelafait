import uuid

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import models
from django.utils.translation import gettext as _, pgettext
from django.template.loader import render_to_string
from django.conf import settings
from django.db import models
from wagtail.models import Site
from wagtail.admin.panels import FieldPanel
from wagtail.admin.mail import send_mail
from wagtail.fields import StreamField
from birdsong.models import Campaign, CampaignStatus, Contact

from .blocks import MailBlocks
from .templatetags.mailing_tags import smooth_timedelta


class MarketingCampaign(Campaign):
    body = StreamField(MailBlocks())
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    panels = Campaign.panels + [
        FieldPanel("body"),
    ]

    class Meta:
        verbose_name = _("Mailing campaign")


class MarketingContact(Contact):
    registration_date = models.DateTimeField(verbose_name=_("Registration date"), auto_now_add=True)
    validated = models.BooleanField(verbose_name=_("Validated"), default=False, blank=True)

    panels = Contact.panels + [
        FieldPanel("validated"),
    ]

    def send_confirmation_mail(self, request):
        if self.validated is False:
            confirm_url = request.build_absolute_uri(
                reverse("mailing_validation", kwargs={"user_id": self.id})
            )
            validity_date = smooth_timedelta(settings.MAILING_CONFIRMATION_VALIDITY)
            if not hasattr(request, "site"):
                site = Site._find_for_request(request)
            else:
                site = request.site
            mail_html_content = render_to_string("mail/confirmation.html",
                                                 {"contact": self,
                                                  "request": request,
                                                  "site": site,
                                                  "link_validity": validity_date,
                                                  "confirm_href": confirm_url})
            mail_txt_content = render_to_string("mail/confirmation.txt",
                                                {"contact": self,
                                                 "request": request,
                                                 "site": site,
                                                 "link_validity": validity_date,
                                                 "confirm_href": confirm_url})

            site = Site.objects.get(is_default_site=True)
            subject = _("Please confirm your subscription")
            send_mail(f"[{site.site_name}] {subject}", message=mail_txt_content,
                      recipient_list=[self.email], html_message=mail_html_content)

    class Meta:
        verbose_name = _("Mailing Contact")
