from django.conf import settings
from django.utils.timezone import now

from django_extensions.management.jobs import DailyJob

from ...models import MarketingContact


class Job(DailyJob):
    help = "Expired mailing contacts prune job"

    def execute(self):
        min_creation_date = now() - settings.MAILING_CONFIRMATION_VALIDITY
        contacts = MarketingContact.objects.filter(validated=False,
                                                  registration_date__lt=min_creation_date)

        contacts.delete()
