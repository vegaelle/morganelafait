from django.conf import settings
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.shortcuts import render, redirect, get_object_or_404, reverse
from wagtail.models import Site
from birdsong.models import CampaignStatus
from birdsong.conf import BIRDSONG_TEST_CONTACT

from mailing.forms import SubscriptionForm

from .models import MarketingContact, MarketingCampaign


def get_fake_page(request, title):
    site = Site._find_for_request(request)
    root_page = site.root_page
    page_obj = {
        "title": title,
        "get_parent": root_page,
        "get_ancestors": [root_page, {}],
        "content_type": {"model": "mailing"},
    }
    return page_obj


def subscription_confirm(request):
    form = None
    if request.method == "POST":
        form = SubscriptionForm(request.POST)
        if form.is_valid():
            form.save(request)
            return redirect("mailing_subscription_success")
    page_obj = get_fake_page(request, _("Mailing list subscription"))
    if contact_id:=request.GET.get("DBG"):
        from .templatetags.mailing_tags import smooth_timedelta
        contact = MarketingContact.objects.get(id=contact_id)
        confirm_url = request.build_absolute_uri(
            reverse("mailing_validation", kwargs={"user_id": contact.id})
        )
        validity_date = smooth_timedelta(settings.MAILING_CONFIRMATION_VALIDITY)
        if not hasattr(request, "site"):
            site = Site._find_for_request(request)
        else:
            site = request.site
        return render(request, "mail/confirmation.html",
                                             {"contact": contact,
                                              "request": request,
                                              "site": site,
                                              "link_validity": validity_date,
                                              "confirm_href": confirm_url})
    return render(request, "mailing/subscription_confirm.html", 
                  context={"page": page_obj, "self": page_obj, "form": form})


def validation(request, user_id):
    page_obj = get_fake_page(request, _("Mailing list confirmation"))
    min_creation_date = now() - settings.MAILING_CONFIRMATION_VALIDITY
    try:
        contact = MarketingContact.objects.get(id=user_id, validated=False,
                                               registration_date__gte=min_creation_date)
    except MarketingContact.DoesNotExist:
        return render(request, "mailing/validation_error.html",
                      context={"page": page_obj, "self": page_obj})
    if request.method == "POST":
        contact.validated = True
        contact.save()
        return redirect(reverse("mailing_validation_confirm",
                                kwargs={"user_id": user_id}))

    return render(request, "mailing/validation.html",
                  context={"contact": contact, "page": page_obj, "self": page_obj })


def validation_confirm(request, user_id):
    page_obj = get_fake_page(request, _("Mailing list confirmation"))
    contact = get_object_or_404(MarketingContact, id=user_id, validated=True)
    return render(request, "mailing/validation_confirm.html",
                  context={"contact": contact, "page": page_obj, "self": page_obj})


def unsubscribe_user(request, user_id):
    page_obj = get_fake_page(request, _("Mailing list management"))
    try:
        contact = MarketingContact.objects.get(id=user_id, validated=True)
    except MarketingContact.DoesNotExist:
        return render(request, "mailing/unsubscribe_user_error.html",
                      context={"page": page_obj, "self": page_obj})
    if request.method == "POST":
        contact.delete()
        return redirect(reverse("mailing_unsubscribe_confirm"))

    page_obj = get_fake_page(request, _("Mailing list management"))
    return render(request, "mailing/unsubscribe_user.html",
                  context={"contact": contact, "page": page_obj, "self": page_obj})


def unsubscribe_user_confirm(request):
    page_obj = get_fake_page(request, _("Mailing list management"))
    return render(request, "mailing/unsubscribe_user_confirm.html",
                  context={"page": page_obj, "self": page_obj})


def campaign_view(request, campaign_uuid):
    campaign = get_object_or_404(MarketingCampaign, uuid=campaign_uuid,
                                 status=CampaignStatus.SENT)
    test_contact = MarketingContact(**BIRDSONG_TEST_CONTACT)
    context = campaign.get_context(request, test_contact)
    context["archive"] = True
    return render(request, campaign.get_template(request), context)
