from typing import Iterable, Any
import itertools
from datetime import timedelta

from django import template
from django.template.defaultfilters import striptags
from django.utils.translation import gettext as _


register = template.Library()


@register.filter
def batched(iterable: Iterable[Any], n: int) -> Iterable[tuple]:
    return itertools.batched(iterable, n)


@register.filter
def plaintext(value: Any) -> str:
    return striptags(str(value)).strip()


@register.filter
def smooth_timedelta(td: timedelta) -> str:
    """Convert a datetime.timedelta object into Days, Hours, Minutes, Seconds."""
    secs = td.total_seconds()
    timetot = []
    args = {}
    if td.days > 1:
        args["days"] = td.days
        if td.days == 1:
            timetot.append(_("%(days)s day"))
        else:
            timetot.append(_("%(days)s days"))

    secs = td.seconds
    if secs > 3600:
        hours = td.seconds // 3600
        args["hours"] = hours
        secs = secs - hours*3600
        if hours > 1:
            timetot.append(_("%(hours)s hours"))
        else:
            timetot.append(_("%(hours)s hour"))

    if secs > 60:
        mins = secs // 60
        args["minutes"] = mins
        secs = secs - mins*60
        if mins > 1:
            timetot.append(_("%(minutes)s minutes"))
        else:
            timetot.append(_("%(minutes)s minute"))

    if secs > 1:
        args["seconds"] = secs
        if secs == 1:
            timetot.append(_("%(seconds)s second"))
        else:
            timetot.append(_("%(seconds)s seconds"))
           
    if len(timetot) == 2:
        td_s = _(" and ").join(timetot)
    else:
        td_s = ", ".join(timetot)
    td_s = td_s % args
    return td_s
