{% load static i18n wagtailcore_tags wagtailimages_tags %}{% with site_name=site.site_name contact_email=contact.email %}{% trans "Hello." %}
{% blocktranslate %}You just subscribed to {{ site_name }} with the address {{ contact_email }}. To confirm your subscription, please click on this link: {{ confirm_link }}.{% endblocktranslate %}
{% trans "If you do not wish to subscribe, or if you weren’t the author of this request, you can safely ignore this message." %}
{% blocktranslate %}If you do not click the above link in {{ link_validity }}, your email will be deleted from our database.{% endblocktranslate %}
{% trans "Best regards," %}
                                                      {{ site_name }}{% endwith %}
